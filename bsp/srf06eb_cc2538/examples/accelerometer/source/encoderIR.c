//******************************************************************************
//      encoderIR.c
//      Contains functions to encode a number in binary to transmit using an
//      infrared LED
//
//      Andre Gaudin MyApiary Ltd 17/01/18
//      Last updated 9/02/18
//******************************************************************************



#include "headers/encoderIR.h"

//******************************************************************************
// Initialise the clock
//******************************************************************************
void initClock(void)
{
  SysCtrlClockSet(false, true, SYS_CTRL_SYSDIV_8MHZ);
  SysCtrlIOClockSet(SYS_CTRL_SYSDIV_8MHZ);
}

//******************************************************************************
// Initialise Timer0 PC2
//******************************************************************************
void initTimer(void)
{
  // Enable Timer 0
  SysCtrlPeripheralEnable(SYS_CTRL_PERIPH_GPT0);
  
  // Configure Timer 0A as an output PWM and Timer 0B as a capture timer
  TimerConfigure(GPTIMER0_BASE, (GPTIMER_CFG_SPLIT_PAIR | GPTIMER_CFG_A_PWM | GPTIMER_CFG_B_CAP_COUNT_UP));
  IOCPinConfigPeriphOutput(GPIO_C_BASE, GPIO_PIN_2, IOC_MUX_OUT_SEL_GPT0_ICP1);
  
  // Set PWM output to PC2
  GPIOPinTypeTimer(GPIO_C_BASE, GPIO_PIN_2);
  IOCPadConfigSet(GPIO_C_BASE, GPIO_PIN_2, IOC_OVERRIDE_OE);
  
 
}

//******************************************************************************
// Check if a certain bit is 0 or 1.
//******************************************************************************
int checkBit(unsigned int number, int bit)
{
  // 8 bits per byte, so check if outside this range.
  if ( bit < 0  || bit > 8 )
  {
    return  0;
  }
  
  // Check if bit is a 1.
  // AND between bit (8 - x) and 1 where x is between 0 and 7.
  // If (8 - x) is set AND 1 then the bit is 1.
  if ( (number >> (8 - bit)) & 0x1 )
  {
    return 1;  
  }
  
  // Otherwise, bit is 0.
  else  
  {
    return 0;
  }
}

//******************************************************************************
// Encodes individual numbers into bytes. Writes to Port C Pin 2 for IR Tx 
// Also flashes LED2 on the SmartRF06.
//******************************************************************************
void encodeData (cplx inputData[BIT_NUMBER / 2])
{
  
  // Note that this encodes so that the first bit is the in the 7th position
  // eg 5 = 10100000 NOT 00000101
  
  // Cycle through each numer in the input arrary.
  for (int i = 0; i < (BIT_NUMBER / 2); i++)
  {         
    // Pulse 3 times
    SysCtrlDelay(DELAY);
    
    // Start bit
    TimerMatchSet(GPTIMER0_BASE, GPTIMER_A, START_BIT_DUTY);
    bspLedSet(BSP_LED_2);
    
    // Enable
    TimerControlLevel(GPTIMER0_BASE, GPTIMER_A, 0);
    TimerEnable(GPTIMER0_BASE, GPTIMER_A);
    //printf("\n Start Encode ");
       
    // Pulse 3 times
     SysCtrlDelay(DELAY); 
    
    // Cycle through each bit in each number.
    for (int bit = 0; bit < 8; bit++)
      {        
        // Check if the bit is a 0 or a 1.
        // If 1, flash the LED.
        if (checkBit(inputData[i], 8 - bit))
        {
          TimerMatchSet(GPTIMER0_BASE, GPTIMER_A, HIGH_BIT_DUTY);
          bspLedSet(BSP_LED_2);
          //printf("%d " , 1);
        }
        
        // Not 1 so a 0. Turn off LED.
        else
        {
          TimerMatchSet(GPTIMER0_BASE, GPTIMER_A, LOW_BIT_DUTY);
          bspLedClear(BSP_LED_2);
          //printf("%d " , 0);
        }
        
        // Pulse 3 times
        SysCtrlDelay(DELAY);
      }
    
    // Stop bit
    TimerMatchSet(GPTIMER0_BASE, GPTIMER_A, END_BIT_DUTY);
    bspLedClear(BSP_LED_2);
    //printf(" End Encode");
    
    // Pulse 3 times
    SysCtrlDelay(DELAY);
    
    // Disable
    TimerDisable(GPTIMER0_BASE, GPTIMER_A);
    TimerControlLevel(GPTIMER0_BASE, GPTIMER_A, 1);
    
    
    // Pulse 3 times
    //SysCtrlDelay(DELAY);
  } 
}