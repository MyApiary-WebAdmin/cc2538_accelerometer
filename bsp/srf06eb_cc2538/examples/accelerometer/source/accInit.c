//******************************************************************************
//! @file       accTest.c
//! @brief      Accelerometer access example for CC2538 on SmartRF06EB. The
//              accelerometer on SmartRF06EB is a Bosch BMA250 with SPI
//              interface.
//
//              Andre Gaudin MyApiary Ltd 12/12/17
//              Last updated 12/01/18
//******************************************************************************
 
#include "headers/accInit.h"

//******************************************************************************
// Globals
//******************************************************************************
static void appUartIsr(void);

// Transmission and receive buffers
static uint8_t pui8TxBuf[16];
static uint8_t pui8RxBuf[16];

//******************************************************************************
// Initialise the board with all required components
//******************************************************************************
void initBoard(void)
{
    // Initialize board
    bspInit(BSP_SYS_CLK_SPD);

    // Initialize SPI interface
    bspSpiInit(BSP_SPI_CLK_SPD);

    // Initialize LCD
    lcdInit();

    // Initialize accelerometer
    accInit();

    // Set LED1 to indicate life
    bspLedSet(BSP_LED_1);

    // Initialize UART to USB MCU
    bspUartBufInit(pui8TxBuf, sizeof(pui8TxBuf), pui8RxBuf, sizeof(pui8RxBuf));

    // Application must register the UART interrupt handler
    UARTIntRegister(BSP_UART_BASE, &appUartIsr);

    // Open UART connection
    if(bspUartOpen(eBaudRate115200) != BSP_UART_SUCCESS)
    {
        // Failed to initialize UART handler
        bspAssert();
    }
}

//******************************************************************************
// Initialise the display
//******************************************************************************
void initDisplay(void)
{   
    // Fill LCD buffer with static text and send to display
    lcdBufferPrintStringAligned(0, "Accelerometer Ex.", eLcdAlignCenter, eLcdPage0);
    lcdBufferInvertPage(0, 0, 127, eLcdPage0);

    lcdBufferPrintString(0, "X-axis:", 0, eLcdPage3);
    lcdBufferPrintString(0, "Y-axis:", 0, eLcdPage4);
    lcdBufferPrintString(0, "Z-axis:", 0, eLcdPage5);
    lcdSendBuffer(0);   
}

/**************************************************************************//**
* @brief    BSP UART interrupt service routine.
*           By default, the BSP UART driver does not allocate ISRs,
*           the application must provide the ISR for the UART RX/TX interrupts.
*           The BSP UART ISR handler must be called in the application's ISR
*           for the processing to be correct.
*
*           If \c BSP_UART_ALLOCATE_ISR and \c BSP_UART_ISR are defined,
*           bspUartOpen() will map bspUartIsrHandler() to the correct UART
*           interrupt vector.
*
*           See bsp.cfg in the bsp source directory for more info.
*
* @return   None.
*
*    Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
*
*    Redistribution and use in source and binary forms, with or without
*    modification, are permitted provided that the following conditions
*    are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
static void appUartIsr(void)
{
    uint32_t ulIntBm = UARTIntStatus(BSP_UART_BASE, 1);
    
    // Serve interrupts handled by BSP UART interrupt handler
    if(ulIntBm & (BSP_UART_INT_BM))
    {
        bspUartIsrHandler();
    }
}