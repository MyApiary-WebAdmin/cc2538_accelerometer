//******************************************************************************
//      userFeedback.c
//      Contains functions to provide the user with feedback. Includes functions
//      for the LED on the SmartRF06 board, IAR IDE's Terminal I/O, and the UART
//      which can be viewed with a terminal such as Terra Terminal etc.
//
//      Andre Gaudin MyApiary Ltd 17/01/18
//      Last updated 17/01/18
//******************************************************************************

#include "headers/userFeedback.h"

//******************************************************************************
// Updates the display with axis data
//******************************************************************************
void updateDisplayAxis(int16_t xAxis, int16_t yAxis, int16_t zAxis, cplx buf[], cplx mag[], int n)
{
    // Clear part of buffer with axis values and update with new ones
    lcdBufferClearPart(0, 10 * LCD_CHAR_WIDTH, 127, eLcdPage3, eLcdPage7);
    lcdBufferPrintInt(0, xAxis, 10 * LCD_CHAR_WIDTH, eLcdPage3);
    lcdBufferPrintInt(0, yAxis, 10 * LCD_CHAR_WIDTH, eLcdPage4);
    lcdBufferPrintInt(0, zAxis, 10 * LCD_CHAR_WIDTH, eLcdPage5);
    
    // Send updated parts of LCD buffer to display
    lcdSendBufferPart(0, 10 * LCD_CHAR_WIDTH, 127, eLcdPage3, eLcdPage7);
} 

//******************************************************************************
// Function to print the transform to terminal I/O
//******************************************************************************
void show(const char * s, cplx buf[], int n) 
{
	printf("%s", s);
	for (int i = 0; i < n; i++)
        {
          
          if (!cimag(buf[i])) 
          {
            printf("%g ", creal(buf[i]));             
          }
          
          else
          {
            printf("(%g, %g) ", creal(buf[i]), cimag(buf[i]));            
          }
          
        }
}

//******************************************************************************
// UART Display Function
//******************************************************************************
void uartDisplay(cplx buf[], int n)
{
    // Define a uint8_t buffer for UART printing
    uint8_t displayBuffer[BIT_NUMBER / 2];
    
    // Store complex numbers as uint8_t numbers 
    for (int i = 0; i < n; i++)
    {
      displayBuffer[i] = buf[i];
      //bspUartDataPut(displayBuffer, 1);
    }

    // Pointer to point to first element in displayBuffer
    uint8_t *displayPointer;
    displayPointer = displayBuffer;
    
    // Print numbers on UART
    bspUartDataPut(displayPointer, n);
}