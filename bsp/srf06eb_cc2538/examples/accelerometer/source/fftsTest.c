//*****************************************************************************
//! @file       fftsTest.c
//! @brief      Accelerometer access example for CC2538 on SmartRF06EB. The
//!             accelerometer on SmartRF06EB is a Bosch BMA250 with SPI
//!             interface.
//!
//!             Andre Gaudin MyApiary Ltd 12/12/17
//              Last updated 07/02/18
//  
//
//  Adapted from Texas Instruments SmartRF06EB CC2538 acc_example
//  Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
//
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions
//  are met:
//
//    Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
//    Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
//    Neither the name of Texas Instruments Incorporated nor the names of
//    its contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//****************************************************************************/

#include "headers/fftTest.h"

// Globals
unsigned long g_ticks = 0;

//******************************************************************************
// Main function which calls the FFT functions
//******************************************************************************
void main(void)
{
    int16_t xAxis, yAxis, zAxis;
   
    // Initialise the board and the display
    initBoard();
    initDisplay();
    initClock();
    initTimer();
    //initInput();

    // Enable global interrutps
    IntMasterEnable();

    while(1)
    {
        // This array holds x axis input
        cplx xAxisData[BIT_NUMBER];
        
        // Used for the magnitude and freq calculations.
        // Over 2 because the FFT results are mirrored.
        cplx magData[BIT_NUMBER / 2];
        cplx freqData[BIT_NUMBER / 2];
        
        int decodedData[BIT_NUMBER];
        
        // Counter counts the number of data entries until the array is full.
        int counter = 0; 
            
        // Poll accelerometer for data.
        for (; counter < BIT_NUMBER; counter++)
        {                                                           
          // Read accelerometer data .
          accReadData(&xAxis, &yAxis, &zAxis);
         
          // Add the x axis value to the array.
          xAxisData[counter] = xAxis;       
        }
        
        // Reset counter to zero for next loop.
        counter = 0; 
        
        // Performthe FFT and subsequently calculate the frequency bins
        // and the magnitudes of each frequency.
        fft(xAxisData, BIT_NUMBER);
        magFFT(xAxisData, magData, (BIT_NUMBER / 2) + 1);
        freqFFT(xAxisData, freqData, (BIT_NUMBER / 2) + 1);
        
        // Send axis position data to LED display
        updateDisplayAxis(xAxis, yAxis, zAxis, xAxisData, magData, BIT_NUMBER);
        
        // Encode data 
        encodeData(magData);
        decodeData(decodedData);
        
        uartDisplay(magData, BIT_NUMBER / 2);
        
        // Send data to UART for display on computer terminal.
        // UART doesn't need to display every cycle as this would
        // be too much information.
        if (g_ticks >= WAIT_TIME)
        {
        
        /* Show functions for debugging on the I/O terminal
        show("\nData: ", xAxisData, BIT_NUMBER);
	show("\nFFT : ", xAxisData, BIT_NUMBER);
        show("\nMag : ", magData, BIT_NUMBER / 2);
        show("\nFrq : ", freqData, BIT_NUMBER / 2); */
        
        /* UART display functions. Prints to terminal
        //! Does not convert to ASCI. Assuming
        //! terminal will do so itself
        uartDisplay(xAxisData, BIT_NUMBER);
        uartDisplay(magData, BIT_NUMBER / 2);
        uartDisplay(freqData, BIT_NUMBER / 2); */
        
        // Reset g_ticks to zero
        g_ticks = 0;
        
        }
        
        // Increase g_ticks. Once g_ticks is high enough,
        // data will be sent to UART for display.
        g_ticks++;
    }
}