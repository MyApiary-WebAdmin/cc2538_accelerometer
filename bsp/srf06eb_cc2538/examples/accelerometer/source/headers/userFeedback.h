//******************************************************************************
//      userFeedback.h
//      Header file for userFeedback.c
//      Contains definitions and defines for use in accInit.c
//
//      Andre Gaudin MyApiary Ltd 17/01/18
//      Last updated 17/01/18
//******************************************************************************

#ifndef USERFEEDBACK_H_
#define USERFEEDBACK_H_

#include "accInit.h"

//******************************************************************************
// Updates the display with axis data
//******************************************************************************
void updateDisplayAxis(int16_t xAxis, int16_t yAxis, int16_t zAxis, cplx buf[], cplx mag[], int n);

//******************************************************************************
// Function to print the transform to terminal I/O
//******************************************************************************
void show(const char * s, cplx buf[], int n);

//******************************************************************************
// UART Display Function
//******************************************************************************
void uartDisplay(cplx buf[], int n);

#endif /* USERFEEDBACK_H */