//******************************************************************************
//      accInit.h
//      Header file for accInit.c
//      Contains definitions and defines for use in accInit.c
//      Also includes the important sample frequency and bit number definitions,
//      as well as the complex structure
//      
//
//      Andre Gaudin MyApiary Ltd 17/01/18
//      Last updated 07/02/18
//******************************************************************************

#ifndef ACCINIT_H_
#define ACCINIT_H_

//******************************************************************************
// Includes
//******************************************************************************
#include "bsp.h"
#include "bsp_uart.h"
#include "bsp_key.h"
#include "bsp_led.h"
#include "acc_bma250.h"
#include "lcd_dogm128_6.h"

#include <uart.h>               // Access to driverlib uart fns
#include <stdio.h>
#include <stdlib.h>
#include <complex.h>

//******************************************************************************
// These are important constants for FFT calculations and other functions
//******************************************************************************
#define BIT_NUMBER      8       // Number of bits (ie, numbers to calculate)
#define SAMPLE_FREQ     250     // FFT sample frequency, Hz

typedef double complex cplx;

//******************************************************************************
// Function Prototypes
//******************************************************************************
void initBoard(void);
void initDisplay(void);
static void appUartIsr(void);

#endif /* ACCINIT_H */