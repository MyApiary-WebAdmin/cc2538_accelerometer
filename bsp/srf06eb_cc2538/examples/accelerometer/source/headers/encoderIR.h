//******************************************************************************
//      encoderIR.h
//      Header file for encoderIR.c
//
//      Andre Gaudin MyApiary Ltd 29/01/18
//      Last updated 07/02/18
//******************************************************************************

#ifndef __ENCODERIR_H__
#define __ENCODERIR_H__

//******************************************************************************
// Includes
//******************************************************************************
#include <stdbool.h>
#include <stdint.h>

#include "hw_gpio.h"
#include "hw_memmap.h"
#include "hw_ioc.h"
#include "hw_ints.h"
#include "interrupt.h"
#include "gpio.h"
#include "ioc.h"
#include "sys_ctrl.h"
#include "gptimer.h"

#include "headers/accInit.h"
#include "headers/userFeedback.h"

//******************************************************************************
// Important constant values for PWM creation
//******************************************************************************
#define START_BIT_DUTY  0xEFFF  // Start bit is at 94% duty cycle
#define HIGH_BIT_DUTY   0x7FFF  // High bit (1) is at 50% duty cycle
#define LOW_BIT_DUTY    0x3FFF  // Low bit (0) is at 25% duty cycle
#define END_BIT_DUTY    0x1FFF  // End bit is at 12% duty cycle
#define DELAY           65448   // 24.543ms

//******************************************************************************
// Prototypes
//******************************************************************************
void outputIntHandler(void);
void initTimer(void);
void initClock(void);
void initInput(void);
int checkBit(unsigned int number, int bit);
void encodeData (cplx inputData[BIT_NUMBER / 2]);

#endif //  __ENCODERIR_H__