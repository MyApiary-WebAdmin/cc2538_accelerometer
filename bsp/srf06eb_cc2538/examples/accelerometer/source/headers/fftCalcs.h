//******************************************************************************
//      fftCalcs.h
//      Header file for fftCalcs.c
//
//      Andre Gaudin MyApiary Ltd 17/01/18
//      Last updated 17/01/18
//******************************************************************************

#ifndef FFTCALCS_H_
#define FFTCALCS_H_

//******************************************************************************
// Includes
//******************************************************************************
#include "accInit.h"
#include <math.h>

//******************************************************************************
// Function prototypes
//******************************************************************************
void _fft(cplx buf[], cplx out[], int n, int step);
void fft(cplx buf[], int n);
void freqFFT(cplx input[], cplx output[], int n);
void magFFT(cplx input[], cplx output[], int n);

#endif /* FFTCALCS_H */