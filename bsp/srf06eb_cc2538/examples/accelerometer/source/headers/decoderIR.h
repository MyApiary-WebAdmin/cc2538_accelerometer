//******************************************************************************
//      decoderIR.h
//      Header file for decoder.c
//
//      Andre Gaudin MyApiary Ltd 06/02/18
//      Last updated 07/02/18
//******************************************************************************

#ifndef __DECODERIR_H__
#define __DECODERIR_H__

//******************************************************************************
// Includes
//******************************************************************************
#include "hw_gpio.h"
#include "hw_memmap.h"
#include "hw_ioc.h"
#include "hw_ints.h"
#include "interrupt.h"
#include "gpio.h"
#include "ioc.h"
#include "sys_ctrl.h"
#include "gptimer.h"

//******************************************************************************
// Prototypes
//******************************************************************************
void initInput(void);
void inputIntHandler(void);
void decodeData(int decodedData[]);

#endif //  __DECODERIR_H__