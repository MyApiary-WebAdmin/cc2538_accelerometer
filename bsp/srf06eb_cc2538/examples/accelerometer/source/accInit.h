//******************************************************************************
//      accInit.h
//      Header file for accInit.c
//      Contains definitions and defines for use in accInit.c
//      Also includes the important sample frequency and bit number definitions,
//      as well as the complex structure
//      
//
//      Andre Gaudin MyApiary Ltd 17/01/18
//      Last updated 17/01/18
//******************************************************************************

#ifndef ACCINIT_H_
#define ACCINIT_H_

#include "bsp.h"
#include "bsp_uart.h"
#include "bsp_key.h"
#include "bsp_led.h"
#include "acc_bma250.h"
#include "lcd_dogm128_6.h"

#include <uart.h>               // Access to driverlib uart fns
#include <stdio.h>
#include <stdlib.h>
#include <complex.h>

#define BIT_NUMBER 32
#define SAMPLE_FREQ 250

typedef double complex cplx;

//******************************************************************************
// Initialise the board with all required components
//******************************************************************************
void initBoard(void);

//******************************************************************************
// Initialise the display
//******************************************************************************
void initDisplay(void);

/**************************************************************************//**
* @brief    BSP UART interrupt service routine.
*           By default, the BSP UART driver does not allocate ISRs,
*           the application must provide the ISR for the UART RX/TX interrupts.
*           The BSP UART ISR handler must be called in the application's ISR
*           for the processing to be correct.
*
*           If \c BSP_UART_ALLOCATE_ISR and \c BSP_UART_ISR are defined,
*           bspUartOpen() will map bspUartIsrHandler() to the correct UART
*           interrupt vector.
*
*           See bsp.cfg in the bsp source directory for more info.
*
* @return   None.
*
*    Adapted from Texas Instruments SmartRF06EB CC2538 acc_example
*    Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
*
*    Redistribution and use in source and binary forms, with or without
*    modification, are permitted provided that the following conditions
*    are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************************************/
static void appUartIsr(void);