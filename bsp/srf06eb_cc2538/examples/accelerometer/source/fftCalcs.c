//******************************************************************************
//      fftCalcs.c
//      Contains functions to calculate the Fast Fourier Transform, as well as
//      the magnitude and frequency of the signal
//
//      Andre Gaudin MyApiary Ltd 17/01/18
//      Last updated 17/01/18
//******************************************************************************

#include "headers/fftCalcs.h"

//******************************************************************************
// Perform the Fast Fourier Transform
//******************************************************************************
void _fft(cplx buf[], cplx out[], int n, int step)
{
        double PI = atan2(1, 1) * 4;
  
	if (step < n) 
        {
          _fft(out, buf, n, step * 2);
          _fft(out + step, buf + step, n, step * 2);
 
          for (int i = 0; i < n; i += 2 * step) 
          {
            cplx t = cexp(-I * PI * i / n) * out[i + step];
            buf[i / 2]     = out[i] + t;
            buf[(i + n)/2] = out[i] - t;
          }
          
	}
}
 
//*****************************************************************************
// Call the FFT and alter the input buffer
//******************************************************************************
void fft(cplx buf[], int n)
{
        // An output array to store the FFT results
	cplx out[BIT_NUMBER];
        
        // Copy the array elements into the new array
	for (int i = 0; i < n; i++) out[i] = buf[i];
 
         //Perform the FFT
	_fft(buf, out, n, 1);
}

//******************************************************************************
// Calculate the frequencies of the FFT
//******************************************************************************
void freqFFT(cplx input[], cplx output[], int n)
{
        cplx j;
        
        // Calculate the frequency bins for every value, excluding the first 
        // because it is 0 (ie, the DC offset is ignored)
        // Highest frequency is half SAMPLE_FREQ, ie the Nyquist rate
        for( int i = 1; i < n; i++)
        {
              j = i * SAMPLE_FREQ / n;
              output[i - 1] = j;
        }
}

//******************************************************************************
// Calculate the magnitude of the frequencies
//******************************************************************************
void magFFT(cplx input[], cplx output[], int n)
{
        // To store real and imaginary numbers
        cplx re;
        cplx im;
        
        // For each result of the FFT,
        // perform out = sqrt(re^2 + im^2)
        for(int i = 1; i < n ; i++)
        {
            // If there is no imag part, sqrt(i * i) = i 
            if (!cimag(input[i]))
            {
                output[i - 1] = creal(input[i]);
            }
            
            // Otherwise, perform sqrt(x * x + y * y)
            else
            {
                re = creal(input[i]);
                im = cimag(input[i]);
                output[i - 1] = sqrt(re * re + im * im);
            }

        }
  
}