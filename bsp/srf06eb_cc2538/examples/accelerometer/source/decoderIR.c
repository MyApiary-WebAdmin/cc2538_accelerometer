//******************************************************************************
//      decoderIR.c
//      Contains functions to decode an infrared signal into a binary number
//
//      Andre Gaudin MyApiary Ltd 06/02/18
//      Last updated 09/02/18
//******************************************************************************

#include "headers/decoderIR.h"
#include "headers/encoderIR.h"

// Globals
int g_countPWM = 0;             // Counts the number of PWM pulses
uint32_t g_dutyCycle = 0;       // Records the current duty cycle
uint32_t g_encodedData[30];     // Stores the raw encoded data
uint32_t g_encodedDataPos = 0;  // Position in the encoded data array

//******************************************************************************
// Initialise GPIO PC3 as an input, and setup an interupt on both edges
//******************************************************************************
void initInput(void)
{
  GPIOPinTypeGPIOInput(GPIO_C_BASE, GPIO_PIN_3);
  GPIOPortIntRegister(GPIO_C_BASE, inputIntHandler);
  GPIOIntTypeSet(GPIO_C_BASE, GPIO_PIN_3, GPIO_BOTH_EDGES);
  GPIOPinIntEnable(GPIO_C_BASE, GPIO_PIN_3);
}

//******************************************************************************
// GPIO PC3 interupt handler. Interupts on both edges
//******************************************************************************
void inputIntHandler(void)
{
  // Disable interupts.
  IntMasterDisable();
  
  // These values store the time the PWM takes to reach
  // the first high edge, first low edge, and second high edge.
  static uint32_t time1;
  static uint32_t time2;
  static uint32_t time1Prev;
  static int firstRisingEdge;
  
  // To store the value of the pin.
  uint32_t pinValue = GPIOPinRead (GPIO_C_BASE, GPIO_PIN_3);
        
  // Clear the interupt flag from PC3.
  GPIOPinIntClear(GPIO_C_BASE, GPIO_PIN_3);
  
  //Low to High transition interrupt.  
  if (pinValue)
  {
    // Update previous time for rising edge, store time for newest rising edge
    time1 = TimerValueGet(GPTIMER0_BASE, GPTIMER_B);

    // Check to see if it is the first interupt after running.
    // This is to prevent a premature calculation without
    // all timed points.  
    if (firstRisingEdge == 0)
    {
      // First occurance rising edge interrupt after running.
      firstRisingEdge = 1;
    }

    else
    {
      // Since there is enough information now, it is possible
      // to calculate the duty cycle for the main rotor.
      if ((time2 > time1Prev) && (time1 > time1Prev))
      {
        // Add duty cycles to matrix.
        g_dutyCycle = ((time2 - time1Prev) * 100) / (time1 - time1Prev);
        g_encodedData[g_encodedDataPos] = g_dutyCycle;
        
        // Add one to the number of PWMs counted.
        g_countPWM++;
        g_encodedDataPos++;
      }
    }
  }
  
  // High to Low transition interrupt.
  else
  {
    // Store time for newest falling edge.
    time1Prev = time1;
    time2 = TimerValueGet(GPTIMER0_BASE, GPTIMER_B);
  }
  
  // Enable interupts.
  IntMasterEnable();
}

//******************************************************************************
// Decode the encoded data and store it in an array
//******************************************************************************
void decodeData(int decodedData[])
{  
  // Flag for errors
  int errorCheck = 0;
    
  // If thirty pulses (ie 10 bits) have been counted.
  if (g_countPWM == 30)
  {
    // Disable interupts to prevent duty cycle matrix from changing.
    IntMasterDisable();
    
    // Check if the first/last three bits are start/stop bits.
    if ((92 < g_encodedData[0] && g_encodedData[1] && g_encodedData[2] < 99)
        && (8 < g_encodedData[27] && g_encodedData[28] && g_encodedData[29] < 14))
    {
      // Counter for en/decoder array position.
      int enPos = 0;
      int dcPos = 0;
      //printf("\n Start Decode ");
        
      // Fill the decoded array with bits.
      while (dcPos <= 8)
      {
        // Check if bit is high.
        if (45 < g_encodedData[enPos] && g_encodedData[enPos + 1] && g_encodedData[enPos + 2] < 55)
        {
          decodedData[dcPos] = 1;
          //printf("1 ");
        }
        
        // Check if bit is low.
        else if (20 < g_encodedData[enPos] && g_encodedData[enPos + 1] && g_encodedData[enPos + 2] < 30)
        {
          decodedData[dcPos] = 0;
          //printf("0 ");
        }
        
        // If it is not high or low an error has occured.
        // Stop adding bits to the decoded array and break
        // out of this while loop.
        else
        {
          errorCheck = 1;
          break;
          //printf("E ");
        }       
        
        // Increase the position varriables
        enPos += 3; // Next bit starts in another 3 pulses
        dcPos += 1; 
       
      }
    }
    
    // If an error has occured, empty the array.
    if (errorCheck == 1)
    {
      // Set every value in the array to 0.
      for (int counter = 0; counter < 8; counter++)
      {
         decodedData[counter] = 0;
      }
    }
    
    // The first/last three bits are not start/stop bits. Remove the first number
    // and move the matrix left by one.
    else
    {
      // Step through each number in the array and move it left by 1.
      for (int counter = 1; counter < g_encodedDataPos; counter++)
      {
         g_encodedData[counter] =  g_encodedData[counter - 1];
      }
      
      // Set the matrix position to 29, so there is space for the next number.
      g_encodedDataPos -= 1;
    }
    
   // Enable next interupt.
   IntMasterEnable();
  }
  // Reset the PWM counter.
  g_countPWM = 0;
}






